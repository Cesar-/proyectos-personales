package archivo_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class creararchivo {
	
	File archivo;
	
	public void crearArchivoTexto() {	
		archivo = new File("prueba.txt");		
		try {
			if(archivo.createNewFile()) {				
				System.out.println("el archivo se creo correctamente");				
			}
		} catch (IOException e) {
			System.err.println("no se pudo crear el archivo" + e);
		}
	}
	
	private void crearDirectorio() {		
		archivo = new File("Directorio");		
		if(archivo.mkdir()) {
			System.out.println("el directorio correctamente");
		}
		else {
			System.out.println("el directorio no se creo");
		}
	}
	
	private void escribirTexto() {
		try {
			FileWriter escribir = new FileWriter(archivo);
			escribir.write("Hola");
			escribir.write("\r\nmas texto");
			escribir.close();
		} catch (IOException e) {
			System.err.println("Error, no se pudo escribir en el archivo" + e);
		}
	}
	
	private void anexarTexto() {		
		try {
			FileWriter escribirT = new FileWriter(archivo, true);
			escribirT.write("\r\nMucho mas texto");
			escribirT.close();	
		} catch (IOException e) {
			System.err.println("no se pudo sibreescribir el archivo" + e);
		}
	}
	
	private void leerTexto() {		
		String cadena;		
		try {
			
			FileReader lector = new FileReader(archivo);
			BufferedReader lectura = new BufferedReader(lector);
			cadena = lectura.readLine();
			while(cadena != null) {
				System.out.println(cadena);
				cadena = lectura.readLine();
			}			
		} catch (FileNotFoundException e) {
			System.err.println("el error es" + e);			
		} catch (IOException e) {
			System.err.println("el error es" + e);
		}		
	}
		
	public static void main(String[] arg) {
		
		creararchivo archivo = new creararchivo();			
			archivo.crearArchivoTexto();
			archivo.crearDirectorio();
			archivo.escribirTexto();
			archivo.anexarTexto();
			archivo.leerTexto();
	}
}
