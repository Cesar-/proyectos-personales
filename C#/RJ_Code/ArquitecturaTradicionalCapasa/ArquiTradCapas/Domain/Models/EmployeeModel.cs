﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class EmployeeModel
    {
        private int idPK;
        private string idNumbre;
        private string name;
        private string mail;
        private DateTime birthday;
        private int age;

        private IEmployeeRepository employeeRepository;
        public EntityState State { private get; set; }


        // Propiedades/ Modelo de vista/ Validacion de datos
        public int IdPK { get => idPK; set => idPK = value; }
        [Required(ErrorMessage ="The field identification numbre is requeried")]
        [RegularExpression("([0-9]+)",ErrorMessage ="Identification number must be only numbers")]
        [StringLength(maximumLength:10,MinimumLength =10,ErrorMessage ="Identification numbermust be 10 digits")]

        public string IdNumbre { get => idNumbre; set => idNumbre = value; }
        [Required]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage ="The field name number must be only letters")]
        [StringLength(maximumLength:100,MinimumLength =3)]

        public string Name { get => name; set => name = value; }
        [Required]

        public string Mail { get => mail; set => mail = value; }
        [Required]
        [EmailAddress]
        public DateTime Birthday { get => birthday; set => birthday = value; }
        public int Age { get => age; private set => age = value; }

        public EmployeeModel()
        {
            employeeRepository = new EmployeeRepository();
        }

        public string SaveChanges()
        {
            string message=null;
            try
            {
                var employeeDataModel = new Employee();
                employeeDataModel.idPK = idPK;
                employeeDataModel.idNumbre = idNumbre;
                employeeDataModel.name = name;
                employeeDataModel.mail = mail;
                employeeDataModel.birthday = birthday;


                // Ejecutar reglas comerciales / calculos
                switch (State)
                {
                    case EntityState.Added:
                        employeeRepository.Add(employeeDataModel);
                        message = "successfull record";
                        break;
                    case EntityState.Modified:
                        employeeRepository.Edit(employeeDataModel);
                        message = "successfull edited";
                        break;
                    case EntityState.Deleted:
                        employeeRepository.Remove(idPK);
                        message = "successfull removed";
                        break;
                }
            }
            catch (Exception ex)
            {
                System.Data.SqlClient.SqlException sqlEx = ex as System.Data.SqlClient.SqlException;
                if(sqlEx!= null && sqlEx.Number == 2627)
                {
                    message = "Duplicate record";
                }
                else
                {
                    message = ex.ToString();
                }
            }
            return message;
        }

        public List<EmployeeModel> GetAll()
        {
            var employeeDataModel = employeeRepository.GetAll();
            var listEmployee = new List<EmployeeModel>();
            foreach(Employee item in employeeDataModel)
            {
                listEmployee.Add(new EmployeeModel
                {
                    idPK = item.idPK,
                    idNumbre = item.idNumbre,
                    name = item.name,
                    mail = item.mail,
                    birthday = item.birthday,
                    age = 
                });
            }
        }
    }
}
