create database MyCompany
go
use MyCompany
go

create table Employee(
IdPK int identity(1,1) primary key,
IdNumber varchar (10) unique not null,
Name nvarchar (80) not null,
Mail nvarchar(150) not null,
Birthday date not null
)

insert into Employee values ('569876121','cesar rivera','cesar@gmail.com','03-01-1996')
insert into Employee values ('569876122','sandra flores','sandra@gmail.com','07-12-1990')
insert into Employee values ('569876123','silvia chavez','silcha@gmail.com','09-03-1998')
insert into Employee values ('569876124','ernesto rivera','neto@gmail.com','1980-09-15')
insert into Employee values ('569876125','gabriel sabas','gabriel@gmail.com','1970-10-02')
insert into Employee values ('569876126','omar vazquez','omarv@gmail.com','1979-01-20')
insert into Employee values ('569876127','pedro solares','pedros@gmail.com','1986-12-10')
insert into Employee values ('569876128','esteban castro','estcastro@gmail.com','1988-07-22')

select * from Employee