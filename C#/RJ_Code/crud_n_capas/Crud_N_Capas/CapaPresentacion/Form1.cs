﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class Form1 : Form

    { 
        CN_Productos objetoCN = new CN_Productos();
        private string idProducto = null;
 
        public Form1()
        {
            InitializeComponent();
        }

        private void MostrarProducto()
        {
            CN_Productos objeto = new CN_Productos();           //se crea un nuevo objeto de la capa de negocio para refrescar los datos del DataGridView
            dataGridView1.DataSource = objeto.MostrarProd();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'practicaDataSet.Productos' Puede moverla o quitarla según sea necesario.
            //this.productosTableAdapter.Fill(this.practicaDataSet.Productos);
            MostrarProducto();

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                objetoCN.InsertarProd(txtNombre.Text, txtDesc.Text, txtMarca.Text, txtPrecio.Text, txtStock.Text);
                MessageBox.Show("Se inserto correctamente los datos");
                MostrarProducto();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo insertar la informacion " + ex);
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count>0)
            {
                txtNombre.Text = dataGridView1.CurrentRow.Cells["Nombre"].Value.ToString();
                txtDesc.Text = dataGridView1.CurrentRow.Cells["Descripcion"].Value.ToString();
                txtMarca.Text = dataGridView1.CurrentRow.Cells["Marca"].Value.ToString();
                txtPrecio.Text = dataGridView1.CurrentRow.Cells["Precio"].Value.ToString();
                txtStock.Text = dataGridView1.CurrentRow.Cells["Stock"].Value.ToString();
                idProducto = dataGridView1.CurrentRow.Cells["id"].Value.ToString();
            }
            else
            {
                MessageBox.Show("Seleccione una fila de la base de datos");
            }
        }
    }
}
