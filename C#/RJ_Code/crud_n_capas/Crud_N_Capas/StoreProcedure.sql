create proc Mostrar_Productos
as
select * from Productos;
go


CREATE proc InsertarProductos
@nombre nvarchar(100),
@desc nvarchar(100),
@marca nvarchar (100),
@precio float,
@stock int
as
insert into Productos values (@nombre,@desc,@marca,@precio,@stock)
go

create proc EditarProductos
@nombre nvarchar(100),
@desc nvarchar(100),
@marca nvarchar (100),
@precio float,
@stock int,
@id int
as
update Productos set Nombre=@nombre, Descripcion=@desc, Marca=@marca, Precio=@precio, Stock=@stock
where id=@id
go

exec Mostrar_Productos;
exec InsertarProductos 'pan','dulce','bombo',12,10;
exec EditarProductos 'leche','1 litro','lala',10,39,3

delete Productos where id=7