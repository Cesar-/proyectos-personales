create database Practica;

use Practica;

create table Productos(
Id int identity (1,1) primary key,
Nombre nvarchar(100),
Descripcion nvarchar (100),
Marca nvarchar (100),
Precio float,
Stock int
)

insert into Productos
values
('Gaseosa','3 litros','coca',7,24),
('Chocolate','Tableta 100 gramos','iberia',12,14),
('Leche','Deslactosada','lala',10,39),
('Zarzamora','Bolsa','estandar',17,20),
('Nectar','1 litros','Jumex',8,12),
('Aceite','2 litros','nutrioli',20,23);

select * from Productos;