﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class CD_Productos
    {

        private CD_Conexcion conexion = new CD_Conexcion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();

        public DataTable Mostrar()
        {
            //metodo transaccional sql
            //comando.Connection = conexion.AbrirConexion();
            //comando.CommandText = "Select * from Productos";
            //leer = comando.ExecuteReader();
            //tabla.Load(leer);
            //conexion.CerrarConexion();
            //return tabla;


            //metodo almacenado sql
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Mostrar_Productos";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(String nombre, string desc, string marca, double precio, int stock)
        {
            //metodo transaccional sql
            //comando.Connection = conexion.AbrirConexion();
            //comando.CommandText = "insert into Productos values('" + nombre + "', '" + desc + "', '" + marca + "', '" + precio + "', '" + stock + "')";
            //comando.ExecuteNonQuery();

            //metodo almacenado sql
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "InsertarProductos";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@nombre", nombre);
            comando.Parameters.AddWithValue("@desc", desc);
            comando.Parameters.AddWithValue("@marca", marca);
            comando.Parameters.AddWithValue("@precio", precio);
            comando.Parameters.AddWithValue("@stock", stock);
            comando.ExecuteNonQuery();
        }

        public void Editar(String nombre, string desc, string marca, double precio, int stock, int id)
        {
            //metodo transaccional sql
            //comando.Connection = conexion.AbrirConexion();
            //comando.CommandText = "update Productos set(@Nombre='"+nombre+"', @Descripcion='"+desc+"', @Marca='"+marca+"', @Precio='"+precio+"', @Stock='"+stock+"', @id='"+id+"')";
            //comando.CommandText = "update Productos set('@Nombre="+nombre+"', '@Descripcion="+desc+"', '@Marca="+marca+"', '@Precio="+precio+"', '@Stock="+stock+"', '@Id="+id+"')";
            //comando.CommandText = "update Productos set('@Nombre','@Descuento', '@Marca', '@Precio', '@Stock', '@id') values ('" + nombre + "', '" + desc + "', '" + marca + "', '" + precio + "', '" + stock + "', '" + id + "')";
            //comando.CommandText = CommandType.Text;
            //comando.ExecuteNonQuery();

            //metodo almacenado sql
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarProductos";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@nombre", nombre);
            comando.Parameters.AddWithValue("@desc", desc);
            comando.Parameters.AddWithValue("@marca", marca);
            comando.Parameters.AddWithValue("@precio", precio);
            comando.Parameters.AddWithValue("@stock", stock);
            comando.Parameters.AddWithValue("@id", id);
            comando.ExecuteNonQuery();
        }
    }
}
