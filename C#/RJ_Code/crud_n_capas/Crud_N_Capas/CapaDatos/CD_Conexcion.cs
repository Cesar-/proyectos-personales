﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class CD_Conexcion
    {
        private SqlConnection Conexcion = new SqlConnection("Server=(local);DataBase=Practica;Integrated Security=true");
        public SqlConnection AbrirConexion()
        {
            if (Conexcion.State == ConnectionState.Closed)
                Conexcion.Open();
            Console.WriteLine("Conexion establecida");
            return Conexcion;
        }
        public SqlConnection CerrarConexion()
        {
            if (Conexcion.State == ConnectionState.Open)
                Conexcion.Close();
            Console.WriteLine("Conexion terminada");
            return Conexcion;
        }
    }
}
