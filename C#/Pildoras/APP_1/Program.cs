﻿using System;

namespace APP_1
{
    class Program
    {
        static void Main(string[] args)
        {
            const double pi = 3.1416; //asignarle el valor en la misma linea que se inicializa

            Console.WriteLine("El radio: ");

            double radio = double.Parse(Console.ReadLine());

            double area = radio * radio * pi;

            Console.WriteLine($"El area del circulo es: {area}");
        }
    }
}
