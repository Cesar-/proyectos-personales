﻿using System;

namespace ambito_sobrecarga
{
    class Program
    {
        int numero1 = 5;
        int numero2 = 3;

        static void Main(string[] args)
        {
            Console.WriteLine(suma(3,6));
        }

         void primerMetodo()
        {
            Console.WriteLine(numero1 + numero2);
        }

        void segundoMetodo()
        {
            Console.WriteLine(numero1 + numero2);
        }

        static int suma(int operador1, int operador2) => operador1 + operador2;
        static int suma(int numero1, int numero2, int numero3) => numero1 + numero2 +numero3;
    }
}
