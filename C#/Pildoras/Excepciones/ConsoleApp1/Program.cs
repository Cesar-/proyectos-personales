﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random numero = new Random();
            int aleatorio = numero.Next(0, 100);
            int minumero;
            int intentos = 0;
            Console.WriteLine("Introduce un numero entre el 0 y el 100");

            do
            {
                intentos++;
                try
                {
                    minumero = int.Parse(Console.ReadLine());
                }
                catch(Exception ex) when (ex.GetType()!= typeof(FormatException))
                {
                    Console.WriteLine(ex.Message);
                    minumero = 0;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    minumero = 0;
                }
                if (minumero > aleatorio) Console.WriteLine("El numero es mas bajo");
                if (minumero < aleatorio) Console.WriteLine("El numero es mas alto");
            } while (aleatorio != minumero);
            Console.WriteLine($"Correcto, lo lograste en {intentos} intenstos");
        }
    }
}
