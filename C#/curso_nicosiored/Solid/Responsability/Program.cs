﻿using System;

namespace Responsability
{
    class Program
    {
        static void Main(string[] args)
        {
            CEmpleado empleado = new CEmpleado("Juan", "Programador", 23, 18000);
            Console.WriteLine(empleado);
            Console.WriteLine(CHacienda.CalcularImpuesto(empleado));
            CHacienda.PagarImpuesto(empleado);
        }
    }
}
