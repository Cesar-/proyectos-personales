﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Responsability
{
    class CHacienda
    {
        public static double CalcularImpuesto(CEmpleado pEmpleado)
        {
            return pEmpleado.Sueldo * 0.35;
        }

        public static void PagarImpuesto(CEmpleado pEmpleado)
        {
            double imp = CalcularImpuesto(pEmpleado);
            Console.WriteLine("se pago {0} en impuestos por parte del empleado {1}", imp, pEmpleado.Nombre);
        }
    }
}
