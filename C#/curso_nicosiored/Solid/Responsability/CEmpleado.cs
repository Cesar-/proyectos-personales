﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Responsability
{
    class CEmpleado

    {
        private string nombre;
        private string puesto;
        private int edad;
        private double sueldo;

        public string Nombre { get => nombre; set => nombre = value; }
        public double Sueldo { get => sueldo; set => sueldo = value; }

        public CEmpleado(string nombre, string puesto, int edad, double sueldo)
        {
            this.nombre = nombre;
            this.puesto = puesto;
            this.edad = edad;
            this.sueldo = sueldo;
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", nombre, puesto, edad, sueldo);
        }

        //estos metodos ya no deben de ir en la clase empleado ya que pertenecen al pago de los impuestos del sueldo
        // y aqui es donde se rompe el principio de unica responsabilidad

        //public double CalcularImpuesto()
        //{
        //    return sueldo * 0.35;
        //}

        //public void PagarImpuesto()
        //{
        //    double imp = CalcularImpuesto();
        //    Console.WriteLine("se pago {0} en impuestos por parte del empleado {1}", imp, nombre);
        //}
    }
}
