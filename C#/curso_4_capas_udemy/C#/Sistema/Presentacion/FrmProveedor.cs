﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class FrmProveedor : Form
    {
        private string NombreAnt;
        
        public FrmProveedor()
        {
            InitializeComponent();
        }

        private void Listar()
        {
            try
            {
                DvgListado.DataSource = NPersona.ListarProveedores();
                Formato();
                Limpiar();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Buscar()
        {
            try
            {
                DvgListado.DataSource = NPersona.BuscarProveedores(TxtBuscar.Text);
                Formato();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
                TxtBuscar.Clear();
                TxtFiltrar.Visible = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Formato()
        {
            DvgListado.Columns[0].Visible = false;
            DvgListado.Columns[1].Width = 50;
            DvgListado.Columns[2].Width = 100;
            DvgListado.Columns[3].Width = 170;
            DvgListado.Columns[3].HeaderText = "Nombre";
            DvgListado.Columns[4].Width = 100;
            DvgListado.Columns[4].HeaderText = "Direccion";
            DvgListado.Columns[5].Width = 120;
        }

        private void Limpiar()
        {
            TxtBuscar.Clear();
            TxtNombre.Clear();
            TxtId.Clear();
            TxtDireccion.Clear();
            TxtTelefono.Clear();
            TxtEmail.Clear();
            BtnInsertar.Visible = true;
            BtnActualizar.Visible = false;
            ErrorIcono.Clear();

            DvgListado.Columns[0].Visible = false;
            BtnEliminar.Visible = false;
            ChkSeleccionar.Checked = false;
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MensajeOk(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FrmProveedor_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (TxtNombre.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados");
                    ErrorIcono.SetError(TxtNombre, "Ingrese un nombre");
                }
                else
                {
                    Rpta = NPersona.Insertar("Proveedor",TxtNombre.Text.Trim(), TxtDireccion.Text.Trim(), TxtTelefono.Text.Trim(), TxtEmail.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se inserto correctamente el registro");
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void DvgListado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Limpiar();
                BtnActualizar.Visible = true;
                BtnInsertar.Visible = false;
                TxtId.Text = Convert.ToString(DvgListado.CurrentRow.Cells["ID"].Value);
                NombreAnt = Convert.ToString(DvgListado.CurrentRow.Cells["Nombre"].Value);
                TxtNombre.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Nombre"].Value);
                TxtDireccion.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Direccion"].Value);
                TxtTelefono.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Telefono"].Value);
                TxtEmail.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Email"].Value);
                TabGeneral.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Selecciona desde la celda nombre " + "| Error: " + ex.Message);
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (TxtId.Text == string.Empty || TxtNombre.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados");
                    ErrorIcono.SetError(TxtNombre, "Ingrese un nombre");
                }
                else
                {
                    Rpta = NPersona.Actualizar(Convert.ToInt32(TxtId.Text),"Proveedor", NombreAnt ,TxtNombre.Text.Trim(), TxtDireccion.Text.Trim(), TxtTelefono.Text.Trim(), TxtEmail.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se actualizo correctamente el registro");
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            TabGeneral.SelectedIndex = 0;
        }

        private void ChkSeleccionar_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkSeleccionar.Checked)
            {
                DvgListado.Columns[0].Visible = true;
                DvgListado.Columns[0].Width = 70;
                BtnEliminar.Visible = true;
            }
            else
            {
                DvgListado.Columns[0].Visible = false;
                BtnEliminar.Visible = false;
            }   
        }

        private void DvgListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == DvgListado.Columns["Seleccionar"].Index)
            {
                DataGridViewCheckBoxCell ChkEliminar = (DataGridViewCheckBoxCell)DvgListado.Rows[e.RowIndex].Cells["Seleccionar"];
                ChkEliminar.Value = !Convert.ToBoolean(ChkEliminar.Value);
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas eliminar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NPersona.Eliminar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se elimino el registro " + Convert.ToString(row.Cells[3].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void TxtFiltrar_Click(object sender, EventArgs e)
        {
            Listar();
            TxtFiltrar.Visible = false;
        }
    }
}
