﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class FrmUsuario : Form
    {
        private string EmailAnt;
        public FrmUsuario()
        {
            InitializeComponent();
        }

        private void Listar()
        {
            try
            {
                DvgListado.DataSource = NUsuario.Listar();
                Formato();
                Limpiar();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Buscar()
        {
            try
            {
                DvgListado.DataSource = NUsuario.Buscar(TxtBuscar.Text);
                Formato();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
                TxtBuscar.Clear();
                TxtFiltrar.Visible = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Formato()
        {
            DvgListado.Columns[0].Visible = false;
            DvgListado.Columns[1].Width = 50;
            DvgListado.Columns[2].Visible = false;
            DvgListado.Columns[3].Width = 120;
            DvgListado.Columns[4].Width = 170;
            DvgListado.Columns[5].Width = 150;
            DvgListado.Columns[5].HeaderText = "Direccion";
            DvgListado.Columns[6].Width = 120;
            DvgListado.Columns[6].HeaderText = "Telefono";
            DvgListado.Columns[7].Visible = false;
        }

        private void Limpiar()
        {
            TxtBuscar.Clear();
            TxtNombre.Clear();
            TxtId.Clear();
            TxtDireccion.Clear();
            TxtTelefono.Clear();
            TxtEmail.Clear();
            TxtClave.Clear();
            BtnInsertar.Visible = true;
            BtnActualizar.Visible = false;
            ErrorIcono.Clear();

            DvgListado.Columns[0].Visible = false;
            BtnActivar.Visible = false;
            BtnDesactivar.Visible = false;
            BtnEliminar.Visible = false;
            ChkSeleccionar.Checked = false;
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MensajeOk(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CargarRol()
        {
            try
            {
                CboRol.DataSource = NRol.Listar();
                CboRol.ValueMember = "idrol";
                CboRol.DisplayMember = "nombre";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            Listar();
            CargarRol();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (CboRol.Text == string.Empty || TxtNombre.Text == string.Empty || TxtEmail.Text == string.Empty || TxtClave.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados");
                    ErrorIcono.SetError(CboRol, "Seleccione un rol");
                    ErrorIcono.SetError(TxtNombre, "Ingrese un nombre");
                    ErrorIcono.SetError(TxtEmail, "Ingrese un email valido");
                    ErrorIcono.SetError(TxtClave, "Ingrese una clave valida");
                }
                else
                {
                    Rpta = NUsuario.Insertar(Convert.ToInt32(CboRol.SelectedValue),TxtNombre.Text.Trim(), TxtDireccion.Text.Trim(),TxtTelefono.Text.Trim(),TxtEmail.Text.Trim(),TxtClave.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se inserto correctamente el registro");
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void DvgListado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DvgListado.SelectedCells.Equals("Administrador"))
                {
                    MessageBox.Show("No se puede alterar a este usuario", "Sistema de venta", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
                else
                {
                    Limpiar();
                    BtnActualizar.Visible = true;
                    BtnInsertar.Visible = false;
                    TxtId.Text = Convert.ToString(DvgListado.CurrentRow.Cells["ID"].Value);
                    CboRol.SelectedValue = Convert.ToString(DvgListado.CurrentRow.Cells["idrol"].Value);
                    TxtNombre.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Nombre"].Value);
                    TxtDireccion.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Direccion"].Value);
                    TxtTelefono.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Telefono"].Value);
                    EmailAnt = Convert.ToString(DvgListado.CurrentRow.Cells["Email"].Value);
                    TxtEmail.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Email"].Value);
                    TabGeneral.SelectedIndex = 1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Seleccione desde la celda nombre" + "| Error: " + ex.Message);
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (TxtId.Text == string.Empty || CboRol.Text == string.Empty || TxtNombre.Text == string.Empty || TxtEmail.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados");
                    ErrorIcono.SetError(CboRol, "Seleccione un rol");
                    ErrorIcono.SetError(TxtNombre, "Ingrese un nombre");
                    ErrorIcono.SetError(TxtEmail, "Ingrese un email valido");
                }
                else
                {
                    Rpta = NUsuario.Actualizar(Convert.ToInt32(TxtId.Text), Convert.ToInt32(CboRol.SelectedValue), TxtNombre.Text.Trim(), TxtDireccion.Text.Trim(), TxtTelefono.Text.Trim(), EmailAnt ,TxtEmail.Text.Trim(), TxtClave.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se actualizo correctamente el registro de usuario");
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            TabGeneral.SelectedIndex = 0;
        }

        private void DvgListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == DvgListado.Columns["Seleccionar"].Index)
            {
                DataGridViewCheckBoxCell ChkEliminar = (DataGridViewCheckBoxCell)DvgListado.Rows[e.RowIndex].Cells["Seleccionar"];
                ChkEliminar.Value = !Convert.ToBoolean(ChkEliminar.Value);
            }
        }

        private void ChkSeleccionar_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkSeleccionar.Checked)
            {
                DvgListado.Columns[0].Visible = true;
                DvgListado.Columns[0].Width = 70;
                BtnActivar.Visible = true;
                BtnDesactivar.Visible = true;
                BtnEliminar.Visible = true;
            }
            else
            {
                DvgListado.Columns[0].Visible = false;
                BtnActivar.Visible = false;
                BtnDesactivar.Visible = false;
                BtnEliminar.Visible = false;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas eliminar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NUsuario.Eliminar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se elimino el registro " + Convert.ToString(row.Cells[4].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnDesactivar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas desactivar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NUsuario.Desactivar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se desactivo el registro " + Convert.ToString(row.Cells[4].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnActivar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas activar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NUsuario.Activar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se activo el registro " + Convert.ToString(row.Cells[4].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void TxtFiltrar_Click(object sender, EventArgs e)
        {
            Listar();
            TxtFiltrar.Visible = false;
        }
    }
}
