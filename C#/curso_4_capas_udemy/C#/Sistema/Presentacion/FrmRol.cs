﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class FrmRol : Form
    {
        public FrmRol()
        {
            InitializeComponent();
        }

        private void Listar()
        {
            try
            {
                DvgListado.DataSource = NRol.Listar();
                Formato();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Formato()
        {
            DvgListado.Columns[0].Width = 100;
            DvgListado.Columns[0].HeaderText = "Id";
            DvgListado.Columns[1].Width = 200;
            DvgListado.Columns[0].HeaderText = "Nombre";
        }

        private void FrmRol_Load(object sender, EventArgs e)
        {
            Listar();
        }

        private void DvgListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
