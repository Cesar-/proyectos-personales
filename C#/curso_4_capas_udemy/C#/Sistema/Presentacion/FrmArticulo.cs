﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace Presentacion
{
    public partial class FrmArticulo : Form
    {
        private string RutaOrigen;
        private string RutaDestino;
        private string Directorio= "D:\\proyectos-personales\\C#\\curso_4_capas_udemy\\Iconos\\Directorio";
        private string NombreAnt;

        public FrmArticulo()
        {
            InitializeComponent();
        }

        private void Listar()
        {
            try
            {
                DvgListado.DataSource = NArticulo.Listar();
                Formato();
                Limpiar();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Buscar()
        {
            try
            {
                DvgListado.DataSource = NArticulo.Buscar(TxtBuscar.Text);
                Formato();
                LblTotal.Text = "Total de registros: " + Convert.ToString(DvgListado.Rows.Count);
                TxtBuscar.Clear();
                TxtFiltrar.Visible = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void Formato()
        {
            DvgListado.Columns[0].Visible = false;
            DvgListado.Columns[0].Width = 100;
            DvgListado.Columns[1].Width = 50;
            DvgListado.Columns[2].Visible = false;
            DvgListado.Columns[3].Width = 100;
            DvgListado.Columns[3].HeaderText = "Categoria";
            DvgListado.Columns[4].Width = 100;
            DvgListado.Columns[4].HeaderText = "Codigo";
            DvgListado.Columns[5].Width = 150;
            DvgListado.Columns[6].HeaderText = "Precio Venta";
            DvgListado.Columns[6].Width = 100;
            DvgListado.Columns[7].Width = 60;
            DvgListado.Columns[8].HeaderText = "Descripcion";
            DvgListado.Columns[8].Width = 200;
            DvgListado.Columns[9].Width = 100;
            DvgListado.Columns[10].Width = 100;
        }

        private void Limpiar()
        {
            TxtBuscar.Clear();
            TxtNombre.Clear();
            TxtNombre.Clear();
            TxtId.Clear();
            TxtCodigo.Clear();
            TxtPrecio.Clear();
            TxtStock.Clear();
            TxtImagen.Clear();
            TxtDescripcion.Clear();
            PicImagen.Image = null;
            PanelCodigo.BackgroundImage = null;            
            BtnGuardarCodigo.Enabled = true;
            BtnInsertar.Visible = true;
            BtnActualizar.Visible = false;
            ErrorIcono.Clear();
            RutaOrigen = "";
            RutaDestino = "";

            DvgListado.Columns[0].Visible = false;
            BtnActivar.Visible = false;
            BtnDesactivar.Visible = false;
            BtnEliminar.Visible = false;
            ChkSeleccionar.Checked = false;
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MensajeOk(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Sistema de ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CargarCategoria()
        {
            try
            {
                CboCategoria.DataSource = NCategoria.Seleccionar();
                CboCategoria.ValueMember = "idCategoria";
                CboCategoria.DisplayMember = "nombre";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void FrmArticulo_Load(object sender, EventArgs e)
        {
            Listar();
            CargarCategoria();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void BtnActivar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas desactivar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NArticulo.Activar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se activo el registro " + Convert.ToString(row.Cells[5].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if(file.ShowDialog() == DialogResult.OK)
            {
                PicImagen.Image = Image.FromFile(file.FileName);
                TxtImagen.Text = file.FileName.Substring(file.FileName.LastIndexOf("\\")+1);
                RutaOrigen = file.FileName;
            }
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
            BarcodeLib.Barcode Codigo = new BarcodeLib.Barcode();
            Codigo.IncludeLabel = true;
            PanelCodigo.BackgroundImage = Codigo.Encode(BarcodeLib.TYPE.CODE128,TxtCodigo.Text,Color.Black,Color.White,400,100);
            BtnGuardarCodigo.Enabled = true;
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardarCodigo_Click(object sender, EventArgs e)
        {
            Image imgFinal = (Image)PanelCodigo.BackgroundImage.Clone();
            SaveFileDialog DialogoGuardar = new SaveFileDialog();
            DialogoGuardar.AddExtension = true;
            DialogoGuardar.Filter = "Image PNG (*.png)|*.png";
            DialogoGuardar.ShowDialog();
            if (!string.IsNullOrEmpty(DialogoGuardar.FileName))
            {
                imgFinal.Save(DialogoGuardar.FileName, ImageFormat.Png);
            }
            imgFinal.Dispose();
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (CboCategoria.Text == string.Empty || TxtNombre.Text == string.Empty || TxtPrecio.Text == string.Empty || TxtStock.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados del formulario");
                    ErrorIcono.SetError(CboCategoria, "Ingresa la categoria del articulo");
                    ErrorIcono.SetError(TxtNombre, "Ingrese el nombre del articulo");
                    ErrorIcono.SetError(TxtPrecio, "Ingresa el precio de venta del articulo");
                    ErrorIcono.SetError(TxtStock, "Ingresa el stock del articulo");
                }
                else
                {
                    Rpta = NArticulo.Insertar(Convert.ToInt32(CboCategoria.SelectedValue),TxtCodigo.Text.Trim(),TxtNombre.Text.Trim(),Convert.ToDecimal(TxtPrecio.Text),Convert.ToInt32(TxtStock.Text),TxtDescripcion.Text.Trim(),TxtImagen.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se inserto correctamente el registro del articulo");
                        if (TxtImagen.Text != string.Empty)
                        {
                            RutaDestino = Directorio + TxtImagen.Text;
                            File.Copy(RutaOrigen,RutaDestino);
                        }
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void TxtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void DvgListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == DvgListado.Columns["Seleccionar"].Index)
            {
                DataGridViewCheckBoxCell ChkEliminar = (DataGridViewCheckBoxCell)DvgListado.Rows[e.RowIndex].Cells["Seleccionar"];
                ChkEliminar.Value = !Convert.ToBoolean(ChkEliminar.Value);
            }
        }

        private void DvgListado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Limpiar();
                BtnActualizar.Visible = true;
                BtnInsertar.Visible = false;
                TxtId.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Id"].Value);
                CboCategoria.SelectedValue = Convert.ToString(DvgListado.CurrentRow.Cells["IdCategoria"].Value);
                TxtCodigo.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Codigo"].Value);
                NombreAnt = Convert.ToString(DvgListado.CurrentRow.Cells["Nombre"].Value);
                TxtNombre.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Nombre"].Value);
                TxtPrecio.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Precio_venta"].Value);
                TxtStock.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Stock"].Value);
                TxtDescripcion.Text = Convert.ToString(DvgListado.CurrentRow.Cells["Descripcion"].Value);
                string Imagen;
                Imagen = Convert.ToString(DvgListado.CurrentRow.Cells["Imagen"].Value);
                if (Imagen != string.Empty)
                {
                    PicImagen.Image = Image.FromFile(Directorio + Imagen);
                    TxtImagen.Text = Imagen;
                }
                else
                {
                    PicImagen.Image = null;
                    TxtImagen.Text = "";
                }
                TabGeneral.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Seleccionar desde la celda nombre " +  ex.Message);
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                string Rpta = "";
                if (TxtId.Text == string.Empty || CboCategoria.Text == string.Empty || TxtNombre.Text == string.Empty || TxtPrecio.Text == string.Empty || TxtStock.Text == string.Empty)
                {
                    MensajeError("Falta ingresar los datos marcados del formulario");
                    ErrorIcono.SetError(CboCategoria, "Ingresa la categoria del articulo");
                    ErrorIcono.SetError(TxtNombre, "Ingrese el nombre del articulo");
                    ErrorIcono.SetError(TxtPrecio, "Ingresa el precio de venta del articulo");
                    ErrorIcono.SetError(TxtStock, "Ingresa el stock del articulo");
                }
                else
                {
                    Rpta = NArticulo.Actualizar(Convert.ToInt32(TxtId.Text),Convert.ToInt32(CboCategoria.SelectedValue),TxtCodigo.Text.Trim(), NombreAnt.Trim(),TxtNombre.Text.Trim(), Convert.ToDecimal(TxtPrecio.Text), Convert.ToInt32(TxtStock.Text), TxtDescripcion.Text.Trim(), TxtImagen.Text.Trim());
                    if (Rpta.Equals("OK"))
                    {
                        MensajeOk("Se actualizo correctamente el registro del articulo");
                        if (TxtImagen.Text != string.Empty && RutaOrigen != string.Empty)
                        {
                            RutaDestino = Directorio + TxtImagen.Text;
                            File.Copy(RutaOrigen, RutaDestino);
                        }
                        Listar();
                        TabGeneral.SelectedIndex = 0;
                    }
                    else
                    {
                        MensajeError(Rpta);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            TabGeneral.SelectedIndex = 0;
        }

        private void ChkSeleccionar_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkSeleccionar.Checked)
            {
                DvgListado.Columns[0].Visible = true;
                DvgListado.Columns[0].Width = 70;
                BtnActivar.Visible = true;
                BtnDesactivar.Visible = true;
                BtnEliminar.Visible = true;
            }
            else
            {
                DvgListado.Columns[0].Visible = false;
                BtnActivar.Visible = false;
                BtnDesactivar.Visible = false;
                BtnEliminar.Visible = false;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas eliminar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    string Imagen = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Imagen = Convert.ToString(row.Cells[9].Value);
                            Rpta = NArticulo.Eliminar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se elimino el registro " + Convert.ToString(row.Cells[5].Value));
                                File.Delete(Directorio + Imagen);
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void BtnDesactivar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente deceas desactivar el/los registro?", "Sistema de ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    int Codigo;
                    string Rpta = "";
                    foreach (DataGridViewRow row in DvgListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToInt32(row.Cells[1].Value);
                            Rpta = NArticulo.Desactivar(Codigo);

                            if (Rpta.Equals("OK"))
                            {
                                MensajeOk("Se desactivo el registro " + Convert.ToString(row.Cells[5].Value));
                            }
                            else
                            {
                                MensajeError(Rpta);
                            }
                        }
                    }
                    Listar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void TxtFiltrar_Click(object sender, EventArgs e)
        {
            Listar();
            TxtFiltrar.Visible = false;
        }
    }
}
