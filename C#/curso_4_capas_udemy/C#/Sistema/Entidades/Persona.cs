﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Persona
    {
        public int IdPersona;
        public string TipoPersona;
        public string Nombre;
        public string Direccion;
        public string Telefono;
        public string Email;
    }
}
