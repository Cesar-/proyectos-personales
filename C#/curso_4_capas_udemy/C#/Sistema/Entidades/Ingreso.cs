﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Entidades
{
    public class Ingreso
    {
        public int IdIngreso { get; set; }

        public int IdProveedor { get; set; }

        public int IdUsuario { get; set; }

        public string TipoComprobante { get; set; }
        
        public string SerieComprovante { get; set; }

        public string NumComprovante { get; set; }

        public DateTime Fecha { get; set; }

        public decimal Impuesto { get; set; }

        public decimal Total { get; set; }

        public string Estdo { get; set; }

        public DataTable Detalles { get; set; }
    }
}
