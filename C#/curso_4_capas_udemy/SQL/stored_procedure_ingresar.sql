
--Procedimiento listar
create proc ingreso_listar
as
select i.idingreso as ID, i.idusuario, u.nombre as Usuario, p.nombre as Proveedor,i.fecha as Fecha, i.impuesto as Impuesto, i.total as Total,
i.estado as Estado
from ingreso i inner join usuario u on i.idusuario=u.idusuario
inner join persona p on i.idproveedor=p.idpersona
order by i.idingreso asc
go

--Procedimiento buscar
create proc ingreso_buscar
@valor varchar(50)
as
select i.idingreso as ID, i.idusuario, u.nombre as Usuario, p.nombre as Proveedor,i.fecha as Fecha, i.impuesto as Impuesto, i.total as Total,
i.estado as Estado
from ingreso i inner join usuario u on i.idusuario=u.idusuario
inner join persona p on i.idproveedor=p.idpersona
where i.idingreso like '%' + @valor + '%' or p.nombre like '%' + @valor + '%' or u.nombre like '%' + @valor + '%'
order by i.fecha asc
go

--Precedimiento anular
create proc ingreso_anular
@idingreso int
as
update ingreso set estado='Anulado'
where idingreso=@idingreso
go

--Crear tipo de dato detalle ingreso
create type type_detalle_ingreso as table
(
	idarticulo int,
	codigo varchar(50),
	articulo varchar(100),
	cantidad int,
	precio decimal(11,2),
	importe decimal(11,2)
)
go

--Precedimiento insertar
create proc ingresar_insertar
@idingreso int output,
@idusuario int,
@idproveedor int,
@impuesto decimal(4,2),
@total decimal (11,2)
as
