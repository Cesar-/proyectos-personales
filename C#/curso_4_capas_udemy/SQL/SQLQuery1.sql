--Crear la base de datos

create database dbsistema
go

--Creacion de tablas

create table categoria(
	idcategoria integer primary key identity,
	nombre varchar(50) not null unique,
	descripcion varchar(255) null,
	estado bit default(1)
);

create table articulo(
	idarticulo integer primary key identity,
	idcategoria integer not null,
	codigo varchar(60) null,
	nombre varchar(100) not null unique,
	precio_venta decimal(11,2) not null,
	stock integer not null,
	descripcion varchar(255) null,
	imagen varchar(30) null,
	estado bit default(1),
	foreign key (idcategoria) references categoria(idcategoria)
);

create table persona(
	idpersona integer primary key identity,
	tipo_persona varchar(20) not null,
	nombre varchar(100) not null,
	direccion varchar(80) null,
	telefono varchar(30) null,
	email varchar(50) null
);

create table rol(
	idrol integer primary key identity,
	nombre varchar(30) not null,
	descripcion varchar(255) null,
	estado bit default(1)
);

create table usuario(
	idusuario integer primary key identity,
	idrol integer not null,
	nombre varchar(100) not null,
	direccion varchar(80) null,
	telefono varchar(30) null,
	email varchar(50) not null,
	clave varchar(50) not null,
	estado bit default(1),
	foreign key (idrol) references rol(idrol) 
);

create table ingreso(
	idingreso integer primary key identity,
	idproveedor integer not null,
	idusuario integer not null,
	tipo_comprobante varchar(30) not null,
	serie_comprobante varchar(10) null,
	num_comprobante varchar(10) not null,
	fecha datetime not null,
	impuesto decimal(4,2) not null,
	total decimal(11,2) not null,
	estado varchar(20) not null
	foreign key (idproveedor) references persona(idpersona),
	foreign key (idusuario) references usuario(idusuario)
	);

create table detalle_ingreso(
	iddetalle_ingreso integer primary key identity,
	idingreso integer not null,
	idarticulo integer not null,
	cantidad integer not null,
	precio decimal(11,2) not null,
	foreign key (idingreso) references ingreso(idingreso) on delete cascade,
	foreign key (idarticulo) references articulo(idarticulo)
);

create table venta(
	idventa integer primary key identity,
	idcliente integer not null,
	idusuario integer not null,
	tipo_comprobante varchar(30) not null,
	serie_comprobante varchar(10) null,
	num_comprobante varchar(10) not null,
	fecha datetime not null,
	impuesto decimal(4,2) not null,
	total decimal(11,2) not null,
	estado varchar(20) not null,
	foreign key (idcliente) references persona(idpersona),
	foreign key (idusuario) references usuario(idusuario)
);


create table detalle_venta(
	iddetalle_venta integer primary key identity,
	idventa integer not null,
	idarticulo integer not null,
	cantidad integer not null,
	precio decimal(11,2) not null,
	descuento decimal(11,2) not null,
	foreign key (idventa) references venta(idventa) on delete cascade,
	foreign key (idarticulo) references articulo(idarticulo)
);


--Llenado de las tablas

insert into categoria (nombre, descripcion) values ('Dispositivos de computo','Todos los dispositivos de computo');


--Select a las tablas
select * from categoria;