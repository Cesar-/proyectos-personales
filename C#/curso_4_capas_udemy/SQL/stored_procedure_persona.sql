--Procedidmiento listar
create proc persona_listar
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
order by idpersona asc
go

--Procedimiento listar proveedores
create proc persona_listar_proveedores
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
where tipo_persona = 'Proveedor'
order by idpersona asc
go

--Procedimiento listar clientes
create proc persona_listar_clientes
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
where tipo_persona = 'Cliente'
order by idpersona asc
go

--Procedidmiento buscar
create proc persona_buscar
@valor varchar(50)
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
where nombre like '%' + @valor + '%' or email like '%' + @valor + '%'
order by idpersona asc
go

--Procedidmiento buscar proveedores
create proc persona_buscar_proveedores
@valor varchar(50)
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
where (nombre like '%' + @valor + '%' or email like '%' + @valor + '%')
and tipo_persona = 'Proveedor'
order by idpersona asc
go

--Procedidmiento buscar clientes
create proc persona_buscar_clientes
@valor varchar(50)
as
select idpersona as ID, tipo_persona as Tipo_Persona, nombre as Nombre,
direccion as Direccion, telefono as Telefono, email as Email
from persona
where (nombre like '%' + @valor + '%' or email like '%' + @valor + '%')
and tipo_persona = 'Cliente'
order by idpersona asc
go

--Procedidmiento insertar
create proc persona_insertar
@tipo_persona varchar(50),
@nombre varchar(50),
@direccion varchar(50),
@telefono varchar(50),
@email varchar(50)
as
insert into persona(tipo_persona,nombre,direccion,telefono,email)
values (@tipo_persona,@nombre,@direccion,@telefono,@email)
go

--Procedidmiento actualizar
create proc persona_actualizar
@idpersona integer,
@tipo_persona varchar(50),
@nombre varchar(50),
@direccion varchar(50),
@telefono varchar(50),
@email varchar(50)
as
update persona set tipo_persona=@tipo_persona,nombre=@nombre,direccion=@direccion,telefono=@telefono, email=@email
where idpersona=@idpersona
go

--Procedidmiento eliminaer
create proc persona_eliminar
@idpersona integer
as
delete from persona 
where idpersona=@idpersona
go

--Procedimiento existe
create proc persona_existe
@valor varchar(100),
@existe bit output
as
if exists (select nombre from persona where nombre = LTRIM(RTRIM(@valor)))
	begin
		set @existe=1
	end
else
	begin
		set @existe=0
	end