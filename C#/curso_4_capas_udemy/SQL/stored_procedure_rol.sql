
-- Insertar roles de usuario

insert into rol (nombre) values ('Administrador')
insert into rol (nombre) values ('Supervisor')
insert into rol (nombre) values ('Vendedor')
go

--Procedimiento listar rol

create proc rol_listar
as
select idrol, nombre from rol
where estado = 1
go 

--Procedimiento listar
create proc usuario_listar
as
select u.idusuario as ID, u.idrol, r.nombre as Rol, u.nombre as Nombre,
u.direccion as Direccion, u.telefono as Telefono, u.email as Email, u.estado as Estado
from usuario u inner join rol r on u.idrol=r.idrol
order by u.idusuario desc
go

--Procedimiento buscar
create proc usuario_buscar
@valor varchar(50)
as
select u.idusuario as ID, u.idrol, r.nombre as Rol, u.nombre as Nombre,
u.direccion as Direccion, u.telefono as Telefono, u.email as Email, u.estado as Estado
from usuario u inner join rol r on u.idrol=r.idrol
where u.nombre like '%' + @valor + '%' or u.email like '%' + @valor + '%'
order by u.nombre asc
go

--Procedimiento insertar
create proc usuario_insertar
@idrol integer,
@nombre varchar(100),
@direccion varchar(70),
@telefono varchar(20),
@email varchar(50),
@clave varchar(50)
as
insert into usuario (idrol,nombre,direccion,telefono,email,clave)
values (@idrol,@nombre,@direccion,@telefono,@email,HASHBYTES('SHA2_256',@clave))
go

--Procedimiento actualizar
create proc usuario_actualizar
@idusuario integer,
@idrol integer,
@nombre varchar(100),
@direccion varchar(70),
@telefono varchar(20),
@email varchar(50),
@clave varchar(50)
as
if @clave<>''
update usuario set idrol=@idrol,nombre=@nombre,direccion=@direccion,telefono=@telefono,email=@email,clave=HASHBYTES('SHA2_256',@clave)
where idusuario=@idusuario;
else
update usuario set idrol=@idrol,nombre=@nombre,direccion=@direccion,telefono=@telefono,email=@email
where idusuario=@idusuario;
go

--Procedimeinto eliminar
create proc usuario_eliminar
@idusuario integer
as
delete from usuario
where idusuario=@idusuario
go

--Procedimiento desactivar
create proc usuario_desactivar
@idusuario integer
as
update usuario set estado=0
where idusuario=@idusuario
go

--Procedimiento activar
create proc usuario_activar
@idusuario integer
as
update usuario set estado=1
where idusuario=@idusuario
go

--Procedimiento existe
create proc usuario_existe
@valor varchar(100),
@existe bit output
as
	if exists (select email from usuario where email =LTRIM(RTRIM(@valor)))
		begin
		set @existe=1
		end
	else
		begin
		set @existe=0
		end

--Procedimiento login
create proc usuario_login
@email varchar(50),
@clave varchar(50)
as
select u.idusuario, u.idrol, r.nombre as rol, u.nombre, u.estado
from usuario u inner join rol r on u.idrol=r.idrol
where u.email = @email and u.clave=HASHBYTES('SHA2_256',@clave)
go