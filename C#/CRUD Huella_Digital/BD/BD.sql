create database login1;
use login1;

create table Horario(
id_Horario char (4) not null,
hora_Entrada datetime,
mi_Tolerancia datetime,
hora_Limite datetime,
hora_Salida datetime,
primary key (id_Horario)
) engine InnoDB;

create table Rol(
id_Rol char(7) not null,
nombre_Rol varchar(50),
primary key (id_Rol)
) engine InnoDB;

create table TipoDoc(
IdTipo int not null,
NombreTipo varchar(50),
Serie varchar(3),
Numero_T varchar(5),
primary key (IdTipo)
) engine InnoDB;

create table Distrito(
Id_Distrito char(7) not null,
Distrito varchar(50) not null,
primary key (Id_Distrito)
) engine InnoDB;

create table Pusuario(
Id_usu char(8) not null,
Nombre_compelto varchar(150),
Avatar varchar(250),
Nom_Usuario varchar(8),
Passwords varchar(8),
Estado_Usu varchar(15),
Id_rol char (7) not null,
primary key (Id_Usu)
) engine InnoDB;

create table Personal(
Id_pernl char(10) not null,
DNIPR char (10),
Nombre_Completo varchar (150),
Fec_Naci date,
Sexo char(1),
Domicilio varchar(120),
Correo varchar(50),
Celular varchar(10),
Id_rol char(7) not null,
Foto varchar (200),
Id_Distrito char(7) not null,
FinguerPrint varbinary (2500),
Estado_Per varchar (20),
primary key (Id_pernl)
) engine InnoDB;

create table Justificacion(
Id_justi char(8) not null,
Id_Pernl char(8) not null,
PrincipalMotivo varchar (50),
Detalle_Justi varchar (500),
FechaJusti date,
EstadoJus varchar (50),
FechaEmi date,
primary key (Id_Justi)
) engine InnoDB;

create table AsistenciaPer(
Id_asis char (8) not null,
Id_Pernl char(8) not null,
FechaAsis date,
Nombre_dia varchar(12) not null,
HoIngreso varchar (10) not null,
HoSalida varchar(10) not null,
Tardanzas real null,
Justificacion varchar (350) null,
EstadoAsis bit null,
primary key (Id_asis)
) engine InnoDB;

alter table Pusuario add
constraint fk_usu_rol foreign key (Id_rol) references Rol (Id_rol);

alter table Personal add
constraint fk_per_rol foreign key (Id_rol) references Rol (Id_rol);

alter table personal add
constraint fk_per_dis foreign key (Id_distrito) references Distrito (Id_distrito);

alter table Justificacion add
constraint fk_justiper foreign key (Id_pernl) references Personal (Id_pernl);

alter table Asistenciaper add
constraint fk_asisPer foreign key (Id_pernl) references personal (Id_pernl);