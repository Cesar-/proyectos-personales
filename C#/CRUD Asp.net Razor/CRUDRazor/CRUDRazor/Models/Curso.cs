﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDRazor.Models
{
    public class Curso
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre del Curso")]
        public string NombreCurso { get; set; }
        [Required]
        [Display(Name = "Nombre del Curso")]
        public int CantidadClases { get; set; }
        [Required]
        public int Precio { get; set; }
    }
}
