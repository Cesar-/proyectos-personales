﻿Module Module1

    Sub Main()

        Console.WriteLine("curso de programacion")

        Dim numero As Integer = 12

        'constantes en mayusculas
        Const PULGADA As Double = 2.54

        Dim letra As Char = "a"

        Dim cadena As String = "cadena de texto"

        Dim condicion As Boolean = True

        Dim nombres As String = "maria"
        Dim Nombre As String = "pablo"

        Dim aritmetico As Double = 8 + 3
        Console.WriteLine(aritmetico)
        Dim aritmetico1 As Double = 8 - 3
        Console.WriteLine(aritmetico1)
        Dim aritmetico2 As Double = 8 * 3
        Console.WriteLine(aritmetico2)
        Dim aritmetico3 As Double = 8 / 3
        Console.WriteLine(aritmetico3)
        Dim aritmetico4 As Double = 8 Mod 3
        Console.WriteLine(aritmetico4)
        Dim aritmetico5 As Boolean = 8 < 3
        Console.WriteLine(aritmetico5)
        Dim aritmetico6 As Boolean = 8 <= 3
        Console.WriteLine(aritmetico6)
        Dim aritmetico7 As Boolean = 8 > 3
        Console.WriteLine(aritmetico7)
        Dim aritmetico8 As Boolean = 8.0! = 3
        Console.WriteLine(aritmetico8)

        Console.ReadKey(True)
    End Sub

End Module
