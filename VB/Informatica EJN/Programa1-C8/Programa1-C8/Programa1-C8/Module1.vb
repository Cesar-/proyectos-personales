﻿Module Module1

    Sub Main()

        Dim valorCompra As Double
        Dim descuentos As Double = 0.0

        Console.WriteLine("Ingrese el valor de la compra")

        valorCompra = Console.ReadLine()

        If valorCompra >= 50 And valorCompra <= 100 Then
            descuentos = valorCompra * 0.05

        ElseIf valorCompra >= 100 And valorCompra <= 150 Then
            descuentos = valorCompra * 0.1

        ElseIf valorCompra >= 150 Then
            descuentos = valorCompra * 0.15
        End If

        Console.WriteLine("El valor total de la compra es: " & valorCompra)
        Console.WriteLine("El valor total de la compra con descuento es: " & valorCompra - descuentos)
        Console.WriteLine("el valor descontado de la compra es: " & descuentos)


        Console.ReadKey()
    End Sub

End Module
