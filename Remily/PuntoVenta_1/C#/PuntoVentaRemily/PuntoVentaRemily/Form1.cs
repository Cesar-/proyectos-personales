﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using LibreriaDLL;

namespace PuntoVentaRemily
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void BtnConexion_Click(object sender, EventArgs e)
        {

            try
            {
                string validar = string.Format("Select * from Usuario where usuario='{0}' and contrasena='{1}'", TxtUsuario.Text.Trim(), TxtPassword.Text.Trim());
                DataSet conectar = Biblioteca.Herramientas(validar);
                int id = Convert.ToInt32(conectar.Tables[0].Rows[0]["id_usuario"].ToString().Trim());
                string usuario = conectar.Tables[0].Rows[0]["nombre"].ToString().Trim();
                string cuenta = conectar.Tables[0].Rows[0]["usuario"].ToString().Trim();
                string contrasena = conectar.Tables[0].Rows[0]["contrasena"].ToString().Trim();
                Boolean permiso = Convert.ToBoolean(conectar.Tables[0].Rows[0]["validar"].ToString());

                if(cuenta == TxtUsuario.Text.Trim() && contrasena == TxtPassword.Text.Trim())
                {
                    MessageBox.Show("Datos correctos \n Inicio de sesion en proceso");
                    TxtPassword.Clear();
                    TxtUsuario.Clear();
                    if (permiso)
                    {
                        MessageBox.Show("el permiso es: " + permiso);
                        MessageBox.Show("El usuario es: " + usuario + "con permisos de " + cuenta);
                        FormAdmin admin = new FormAdmin();
                        this.Hide();
                        admin.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("el permiso es: " + permiso);
                        MessageBox.Show("El usuario es: " + usuario + " con permisos de " + cuenta);
                        FormUser user = new FormUser();
                        this.Hide();
                        user.ShowDialog();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Datos incorrectos \n Usuario o contraseña invalidos \n  Intente nuevamente \n" + ex.Message);
                TxtPassword.Clear();
                TxtUsuario.Clear();
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Va a salir del programa");
            this.Close();
            //Application.Exit();
        }

        private void FormLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }
    }
}
