﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoVentaRemily
{
    public partial class FormAdmin : Form
    {
        public FormAdmin()
        {
            InitializeComponent();
            LblAdmin.Text = ("Usuario: admin");
        }

        private void BtnAdminSalir_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Salir de la app");
            this.Close();
            Application.Exit();
        }

        private void BtnAdminCSes_Click(object sender, EventArgs e)
        {
            FormLogin login = new FormLogin();
            this.Hide();
            login.ShowDialog();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormDatos datos = new FormDatos();
            datos.ShowDialog();
        }

        private void FormAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }
    }
}
