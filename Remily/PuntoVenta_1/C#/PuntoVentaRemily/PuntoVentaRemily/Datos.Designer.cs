﻿
namespace PuntoVentaRemily
{
    partial class FormDatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblDaUsername = new System.Windows.Forms.Label();
            this.LblDaAccount = new System.Windows.Forms.Label();
            this.LblDaPassword = new System.Windows.Forms.Label();
            this.LblDaPermissions = new System.Windows.Forms.Label();
            this.BtnDaSalir = new System.Windows.Forms.Button();
            this.BtnDaAceptar = new System.Windows.Forms.Button();
            this.BtnDaBorrar = new System.Windows.Forms.Button();
            this.TxtDaUsername = new System.Windows.Forms.TextBox();
            this.TxtDaAccount = new System.Windows.Forms.TextBox();
            this.TxtDaPassword = new System.Windows.Forms.TextBox();
            this.TxtDaPermissions = new System.Windows.Forms.TextBox();
            this.TxtDaId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LblDaUsername
            // 
            this.LblDaUsername.AutoSize = true;
            this.LblDaUsername.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDaUsername.Location = new System.Drawing.Point(23, 37);
            this.LblDaUsername.Name = "LblDaUsername";
            this.LblDaUsername.Size = new System.Drawing.Size(84, 22);
            this.LblDaUsername.TabIndex = 0;
            this.LblDaUsername.Text = "Username";
            // 
            // LblDaAccount
            // 
            this.LblDaAccount.AutoSize = true;
            this.LblDaAccount.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDaAccount.Location = new System.Drawing.Point(26, 88);
            this.LblDaAccount.Name = "LblDaAccount";
            this.LblDaAccount.Size = new System.Drawing.Size(72, 22);
            this.LblDaAccount.TabIndex = 1;
            this.LblDaAccount.Text = "Account";
            // 
            // LblDaPassword
            // 
            this.LblDaPassword.AutoSize = true;
            this.LblDaPassword.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDaPassword.Location = new System.Drawing.Point(26, 137);
            this.LblDaPassword.Name = "LblDaPassword";
            this.LblDaPassword.Size = new System.Drawing.Size(80, 22);
            this.LblDaPassword.TabIndex = 2;
            this.LblDaPassword.Text = "Password";
            // 
            // LblDaPermissions
            // 
            this.LblDaPermissions.AutoSize = true;
            this.LblDaPermissions.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDaPermissions.Location = new System.Drawing.Point(26, 188);
            this.LblDaPermissions.Name = "LblDaPermissions";
            this.LblDaPermissions.Size = new System.Drawing.Size(139, 22);
            this.LblDaPermissions.TabIndex = 3;
            this.LblDaPermissions.Text = "User permissions";
            // 
            // BtnDaSalir
            // 
            this.BtnDaSalir.Font = new System.Drawing.Font("MV Boli", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDaSalir.Location = new System.Drawing.Point(29, 281);
            this.BtnDaSalir.Name = "BtnDaSalir";
            this.BtnDaSalir.Size = new System.Drawing.Size(100, 30);
            this.BtnDaSalir.TabIndex = 4;
            this.BtnDaSalir.Text = "Salir";
            this.BtnDaSalir.UseVisualStyleBackColor = true;
            this.BtnDaSalir.Click += new System.EventHandler(this.BtnDaSalir_Click);
            // 
            // BtnDaAceptar
            // 
            this.BtnDaAceptar.Font = new System.Drawing.Font("MV Boli", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDaAceptar.Location = new System.Drawing.Point(197, 281);
            this.BtnDaAceptar.Name = "BtnDaAceptar";
            this.BtnDaAceptar.Size = new System.Drawing.Size(100, 30);
            this.BtnDaAceptar.TabIndex = 5;
            this.BtnDaAceptar.Text = "Aceptar";
            this.BtnDaAceptar.UseVisualStyleBackColor = true;
            this.BtnDaAceptar.Click += new System.EventHandler(this.BtnDaAceptar_Click);
            // 
            // BtnDaBorrar
            // 
            this.BtnDaBorrar.Font = new System.Drawing.Font("MV Boli", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDaBorrar.Location = new System.Drawing.Point(357, 281);
            this.BtnDaBorrar.Name = "BtnDaBorrar";
            this.BtnDaBorrar.Size = new System.Drawing.Size(100, 30);
            this.BtnDaBorrar.TabIndex = 6;
            this.BtnDaBorrar.Text = "Borrar";
            this.BtnDaBorrar.UseVisualStyleBackColor = true;
            // 
            // TxtDaUsername
            // 
            this.TxtDaUsername.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDaUsername.Location = new System.Drawing.Point(207, 38);
            this.TxtDaUsername.Name = "TxtDaUsername";
            this.TxtDaUsername.Size = new System.Drawing.Size(225, 35);
            this.TxtDaUsername.TabIndex = 7;
            this.TxtDaUsername.TextChanged += new System.EventHandler(this.TxtDaUsername_TextChanged);
            // 
            // TxtDaAccount
            // 
            this.TxtDaAccount.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDaAccount.Location = new System.Drawing.Point(207, 85);
            this.TxtDaAccount.Name = "TxtDaAccount";
            this.TxtDaAccount.Size = new System.Drawing.Size(225, 35);
            this.TxtDaAccount.TabIndex = 8;
            // 
            // TxtDaPassword
            // 
            this.TxtDaPassword.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDaPassword.Location = new System.Drawing.Point(207, 134);
            this.TxtDaPassword.Name = "TxtDaPassword";
            this.TxtDaPassword.Size = new System.Drawing.Size(225, 35);
            this.TxtDaPassword.TabIndex = 9;
            // 
            // TxtDaPermissions
            // 
            this.TxtDaPermissions.Font = new System.Drawing.Font("MV Boli", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDaPermissions.Location = new System.Drawing.Point(207, 185);
            this.TxtDaPermissions.Name = "TxtDaPermissions";
            this.TxtDaPermissions.Size = new System.Drawing.Size(225, 35);
            this.TxtDaPermissions.TabIndex = 10;
            // 
            // TxtDaId
            // 
            this.TxtDaId.Location = new System.Drawing.Point(207, 10);
            this.TxtDaId.Name = "TxtDaId";
            this.TxtDaId.Size = new System.Drawing.Size(100, 22);
            this.TxtDaId.TabIndex = 11;
            // 
            // FormDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(514, 339);
            this.Controls.Add(this.TxtDaId);
            this.Controls.Add(this.TxtDaPermissions);
            this.Controls.Add(this.TxtDaPassword);
            this.Controls.Add(this.TxtDaAccount);
            this.Controls.Add(this.TxtDaUsername);
            this.Controls.Add(this.BtnDaBorrar);
            this.Controls.Add(this.BtnDaAceptar);
            this.Controls.Add(this.BtnDaSalir);
            this.Controls.Add(this.LblDaPermissions);
            this.Controls.Add(this.LblDaPassword);
            this.Controls.Add(this.LblDaAccount);
            this.Controls.Add(this.LblDaUsername);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDatos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Datos de Usuario";
            this.Load += new System.EventHandler(this.FormDatos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblDaUsername;
        private System.Windows.Forms.Label LblDaAccount;
        private System.Windows.Forms.Label LblDaPassword;
        private System.Windows.Forms.Label LblDaPermissions;
        private System.Windows.Forms.Button BtnDaSalir;
        private System.Windows.Forms.Button BtnDaAceptar;
        private System.Windows.Forms.Button BtnDaBorrar;
        private System.Windows.Forms.TextBox TxtDaUsername;
        private System.Windows.Forms.TextBox TxtDaAccount;
        private System.Windows.Forms.TextBox TxtDaPassword;
        private System.Windows.Forms.TextBox TxtDaPermissions;
        private System.Windows.Forms.TextBox TxtDaId;
    }
}