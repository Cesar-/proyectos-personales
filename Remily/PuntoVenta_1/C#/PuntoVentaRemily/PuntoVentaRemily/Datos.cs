﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibreriaDLL;

namespace PuntoVentaRemily
{
    public partial class FormDatos : Form
    {
        public FormDatos()
        {
            InitializeComponent();
        }

        private void BtnDaAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                string validar = string.Format("Update Usuario set nombre =" + TxtDaUsername.ToString() +
                    "usuario =" + TxtDaAccount.ToString() + "contrasena =" + TxtDaPassword.ToString() +
                    "where id_Uduario =" + TxtDaId.ToString());
                DataSet conectar = Biblioteca.Herramientas(validar);
                MessageBox.Show("Datos cambiados");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            /*int id = Convert.ToInt32(conectar.Tables[0].Rows[0]["id_Usuario"].ToString().Trim());
            //string usuario = conectar.Tables[0].Rows[0]["nombre"].ToString().Trim();
            //string cuenta = conectar.Tables[0].Rows[0]["usuario"].ToString().Trim();
            //string contrasena = conectar.Tables[0].Rows[0]["contrasena"].ToString().Trim();
            //Boolean permiso = Convert.ToBoolean(conectar.Tables[0].Rows[0]["validar"].ToString());
            //LblDaId.Text = id.ToString();
            //TxtDaUsername.Text = usuario.ToString();
            //TxtDaAccount.Text = cuenta.ToString();
            //TxtDaPassword.Text = contrasena.ToString();
            TxtDaPermissions.Text = permiso.ToString();*/
        }

        private void FormDatos_Load(object sender, EventArgs e)
        {
            string validar = string.Format("Select * from Usuario");
            DataSet conectar = Biblioteca.Herramientas(validar);
            int id = Convert.ToInt32(conectar.Tables[0].Rows[0]["id_Usuario"].ToString().Trim());
            string usuario = conectar.Tables[0].Rows[0]["nombre"].ToString().Trim();
            string cuenta = conectar.Tables[0].Rows[0]["usuario"].ToString().Trim();
            string contrasena = conectar.Tables[0].Rows[0]["contrasena"].ToString().Trim();
            Boolean permiso = Convert.ToBoolean(conectar.Tables[0].Rows[0]["validar"].ToString());
            TxtDaId.Text = id.ToString();
            TxtDaUsername.Text = usuario.ToString();
            TxtDaAccount.Text = cuenta.ToString();
            TxtDaPassword.Text = contrasena.ToString();
            TxtDaPermissions.Text = permiso.ToString();
        }

        private void BtnDaSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtDaUsername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
