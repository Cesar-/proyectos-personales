﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoVentaRemily
{
    public partial class FormUser : Form
    {
        public FormUser()
        {
            InitializeComponent();
            LblUser.Text = ("Usuario: supervisor");
        }

        private void BtnUserSalir_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Salir de la app");
            this.Close();
            Application.Exit();
        }

        private void BtnUserCSes_Click(object sender, EventArgs e)
        {
            FormLogin login = new FormLogin();
            this.Hide();
            login.ShowDialog();
            this.Close();
        }

        private void LblUser_Click(object sender, EventArgs e)
        {
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormDatos datos = new FormDatos();
            datos.ShowDialog();
        }

        private void FormUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }
    }
}
