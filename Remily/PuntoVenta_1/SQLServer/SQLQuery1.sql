create database PVRemily
use PVRemily

create table Cliente(
id_cliente int identity(1,1) primary key,
nombre varchar(100) not null,
apellido varchar(50) not null
)

create table Producto(
id_Producto int identity(1,1) primary key,
nombre varchar(50) not null,
precio float not null
)

create table Usuario(
id_Usuario int identity(1,1) primary key,
nombre varchar(20) not null unique,
usuario varchar(30) not null unique,
contrasena varchar(30) not null unique,
validar bit not null
)