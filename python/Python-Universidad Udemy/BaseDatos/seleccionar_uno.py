import psycopg2
from psycopg2 import connect

conexion = psycopg2.connect(user='postgres', 
                 password='root', 
                 host='localhost',
                 port='5432',
                 database='test_db'
                 )

cursor = conexion.cursor()
sql = 'select * from persona where id_persona = %s'
id_persona = 2
llave_primaria=(id_persona,)
cursor.execute(sql, llave_primaria)

registros = cursor.fetchone()   #fetchone() regresa solo un registro
#registros = cursor.fetchall() fetchall() regresa todos los registros

print(registros)

cursor.close()
conexion.close()