import psycopg2
from psycopg2 import connect

conexion = psycopg2.connect(user='postgres', 
                 password='root', 
                 host='localhost',
                 port='5432',
                 database='test_db'
                 )

cursor = conexion.cursor()
sql = 'select * from persona order by id_persona desc'

cursor.execute(sql)

registros = cursor.fetchall()

print(registros)

cursor.close()
conexion.close()