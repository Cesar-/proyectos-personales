nombres = ["juan", "carla", "ricardo", "maria"]
print(nombres)
#el largo de la lista
print(len(nombres))
#navegacion inversa
print(nombres[-1])
print(nombres[-2])
#imprimir rango sin incluir el rango de referencia superior
print(nombres[0:2])
#imprimir los elementos de inicio sin incluir el rango de referencia inferior
print(nombres[:3])
#imprimir los indices finales proporcionando el indice inferior
print(nombres[1:])
#cambiar los elementos de la lista
nombres[3] = "Ivonne"
print(nombres)
#iterar los elementos de la lista con un foreach
for nombre in nombres:
    print(nombre)
#iterar si un elemento existe con foreach
if "carla" in nombres:
    print("carla si existe en la lista")
else:
    print("el elemento buscado no existe en la busqueda")    
#agregar un nuevo elemento
nombres.append("Lorenzo")
print(nombres)
#agregar una elemento a la lista en el indice proporcionado
nombres.insert(1,"octavio")
print(nombres)
#remover un elemento de la lsita
nombres.remove("octavio")
print(nombres)
#remover el ultimo elemento de la lista agregado
nombres.pop()
print(nombres)
#remover el indice indicado de la lista
del nombres[0]
print(nombres)
#vaciar o limpiar la lista
nombres.clear()
print(nombres)
#eliminar la lista
del nombres
print(nombres)