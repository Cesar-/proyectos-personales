#un diccionario esta compuesto de elemento de tipo key y value
diccionario = {
    "ide":"integrated development environment",
    "oop":"objetc oriented programming",
    "dbms":"database management system"
}
print(diccionario)
#largo del diccionario
print(len(diccionario))
#acceder a un elemento en especifico ya que no existen indices
print(diccionario["ide"])
#otra forma de acceder a un elemento en especifico con el emtodo get
print(diccionario.get("oop"))
#modificar un valor en especifico
diccionario["ide"] = "integrated Development"
print(diccionario)
#iterar los elementos, solo imprime las key
for termino in diccionario:
    print(termino)
# iterar elementos key y value
for termino in diccionario:
    print(termino)
for termino in diccionario:
    print(diccionario[termino])
#iterar elementos, solo imprima value
for valores in diccionario.values():
    print(valores)
    #comprobando existencia
    print("Ie" in diccionario)
#agregar un elemento al diccionario
diccionario["pk"] = "primary key"
print(diccionario)
#limpiar diccionario
diccionario.clear()
print(diccionario)
#eliminar diccionario
del diccionario
print(diccionario)