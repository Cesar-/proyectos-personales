#set es una coleccion sin orden ni indices ni elementos repetidos y los elementos no se pueden modificar pero si agregar o editar
planetas = {"marte", "jupiter", "venus"}
print(planetas)
#largo del set
print(len(planetas))
#verificar si existe un elemento en el set
print("marte" in planetas)
#agregar nuevos elementos
planetas.add("tierra")
print(planetas)
#un set no puede tener elementos duplicados
planetas.add("tierra")
print(planetas)
#eliminar elementos
planetas.remove("tierra")
print(planetas)
#eliminar con discard no lanza una excepcion si no encuentra el elemento
planetas.discard("jupiters")
print(planetas)
#limpiar el set
planetas.clear()
print(planetas)
#eliminar el set
del planetas
print(planetas)