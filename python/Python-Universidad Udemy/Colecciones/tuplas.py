#tupla mantiene le orden pero no se pude modificar los elementos de la tupla
frutas = ("naranja", "platano", "guayaba")
print(frutas)
#largo de la tupla
print(len(frutas))
#acceder a un elemento
print(frutas[0])
#navegacion inversa
print(frutas[-1])
#rangos especificos excluyendo el indice mayor
print(frutas[0:-1])
#tratar de modificar un elemento de la tupla
#frutas[0] = "naranjita"
#hacer la convercion de tupla a lista para hacer cambios en la tupla
frutasLista = list(frutas)
frutasLista[0] = "naranjita"
print(frutasLista)
#hacer la convercion de lista a tupla
frutas = tuple(frutasLista)
print(frutas)
#iterar una tupla
for fruta in frutas:
    print(fruta,end=" ")
#no se puede agregar ni eliminar elementos de la tupla
del frutas
print(frutas)