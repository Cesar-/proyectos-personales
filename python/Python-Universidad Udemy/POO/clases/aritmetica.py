class Aritmetica:
    def __init__(self, operando1, operando2):
        self.operando1 = operando1
        self.operando2 = operando2
        
    def sumar(self):
        return self.operando1 + self.operando2
    def restar(self):
        return self.operando1 - self.operando2
    def multiplicar(self):
        return self.operando1 * self.operando2
    def dividir(self):
        return self.operando1 / self.operando2
    
#crear un objeto de la clase para la suma
aritmetica = Aritmetica(2,4)
print("resultado de la suma: ", aritmetica.sumar())
#crear un objeto de la clase para la resta
aritmetica = Aritmetica(2,4)
print("resultado de la resta: ", aritmetica.restar())
#crear un objeto de la clase para la multiplicacion
aritmetica = Aritmetica(2,4)
print("resultado de la multiplicacion: ", aritmetica.multiplicar())
#crear un objeto de la clase para la division
aritmetica = Aritmetica(2,4)
print("resultado de la division: ", aritmetica.dividir())
