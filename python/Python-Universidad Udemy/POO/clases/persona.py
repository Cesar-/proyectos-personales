class Persona:
    
    def __init__(self, nombre, edad):
        self.nombre= nombre
        self.edad = edad
#modificar valores
Persona.nombre = "cesar"
Persona.edad = 28
#acceder a los valores
print(Persona.nombre)
print(Persona.edad)
#crear un objeto de la clase persona
persona = Persona("carlos", 30)
print(persona.nombre)
print(persona.edad)
print(id(persona))
#creacion de otro objeto
persona2 = Persona("Carla", 19)
print(persona2.nombre)
print(persona2.edad)
print(id(persona2))