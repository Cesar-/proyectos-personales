class Persona(object):
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
        
    def __str__(self):
        return "nombre: " + self.nombre + " edad: " + str(self.edad)

class Empleado(Persona):
    def __init__(self, nombre, edad, sueldo):
        super().__init__(nombre, edad)
        self.sueldo = sueldo
    def __str__(self):
        return super().__str__() + " sueldo: " + str(self.sueldo) 
        
persona = Persona("juan", 20)
print(persona)

empleado = Empleado("carla", 30, 500.00)
print(empleado)

empleado.nombre = "calrla ivonne"
print(empleado)