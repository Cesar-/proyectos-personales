
class Persona:
    def __init__(self, nombre, appelido_paterno, apellido_materno):
        self.nombre = nombre                                        #publico
        self._apellido_paterno = appelido_paterno                   #protected  _
        self.__apellido_materno = apellido_materno                  #privado    __
        
    def metodo_publico(self):
        self.__metodo_privado()
    
    def __metodo_privado(self):
        print(self.nombre)
        print(self._apellido_paterno)
        print(self.__apellido_materno)
    
    def get_materno(self, ):
        return self.__apellido_materno
    def set_materno(self, materno):
        self.__apellido_materno = materno
    
p1 = Persona("juan", "lopez", "perez")
p1.metodo_publico()

print(p1.nombre)
print(p1._apellido_paterno)
print(p1.get_materno())