
#ABC para clases abstractas

from abc import ABC, abstractmethod

class FiguraGeometrica:
    def __init__(self, ancho, largo):
        self.ancho = ancho
        self.largo = largo
    
    @abstractmethod
    def area(self):
        pass