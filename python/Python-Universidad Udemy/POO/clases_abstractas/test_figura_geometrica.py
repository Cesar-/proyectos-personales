
from cuadrado import Cuadrado
from clases_abstractas.figura_geometrica import FiguraGeometrica

figurageometrica = FiguraGeometrica()

cuadrado = Cuadrado(4,"rojo")
print(cuadrado.area())
print(cuadrado.color)

print(Cuadrado.mro())