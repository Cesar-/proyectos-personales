
from clases_abstractas.figura_geometrica import FiguraGeometrica
from clases_abstractas.color import Color

class Cuadrado(FiguraGeometrica, Color):
    def __init__(self, lado, color):
        FiguraGeometrica.__init__(self, lado, lado)
        Color.__init_(self, color)
        
    def area(self):
        return self.alto * self.ancho