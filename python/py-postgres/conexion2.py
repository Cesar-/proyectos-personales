import psycopg2

#constantes de conexion
PSQL_HOST = "localhost"
PSQL_PORT = "5432"
PSQL_USER = "postgres"
PSQL_PASS = "root"
PSQL_BD = "bd1"

#conexion
connection_address = """
host=%s port=%s user=%s password=%s dbname=%s
""" % (PSQL_HOST,PSQL_PORT,PSQL_USER,PSQL_PASS,PSQL_BD)
connection = psycopg2.connect(connection_address)

cursor = connection.cursor()

#query
sql = "select * from alumno;"
cursor.execute(sql)

#obtener valores
all_values = cursor.fetchall()

cursor.close()
connection.close()

print('todos los valores', all_values)
