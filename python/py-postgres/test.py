import psycopg2

#crear conexion
conn = psycopg2.connect(
    host = "localhost",
    database = "bd1",
    user = 'postgres',
    password = 'root'
    )

print("conexion exitosa")
#cerar cursor para postgre
cursor = conn.cursor()

#ejecutar solo la primera vez
cursor.execute("drop table if exists alumno")

#crear la tabla alumno
sql = """create table alumno(
        nombre char(100) not null,
        apellido_paterno char(100) not null,
        apellido_materno char(100),
        edad int,
        sexo char(1),
        calificacion float
)
"""

cursor.execute(sql)

conn.commit()

print("tabla creada con exito")

#cerrar la conexion

conn.close()
