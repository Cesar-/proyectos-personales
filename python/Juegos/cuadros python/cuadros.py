import pygame
import sys
import random

#constantes de tamaño
ANCHO = 800
LARGO = 600
COLOR_ROJO = (255,0,0)
COLOR_NEGRO = (0,0,0)
COLOR_AZUL = (0,0,255)

#jugador
JUGADOR_POS = [ANCHO / 2,LARGO - 100]
JUGADOR_SIZE = 50

#enemigo
ENEMIGO_SIZE = 50
ENEMIGO_POS = [random.randint(0,ANCHO - ENEMIGO_SIZE),0]

#crear el popup
ventana = pygame.display.set_mode((ANCHO,LARGO))

game_over = False
clock = pygame.time.Clock()

def detectar_colision(JUGADOR_POS, ENEMIGO_POS):
	jx = JUGADOR_POS[0]
	jy = JUGADOR_POS[1]
	ex = ENEMIGO_POS[0]
	ey = ENEMIGO_POS[1]

	if (ex >= jx and ex <(jx + JUGADOR_SIZE)) or (jx >= ex and jx <(ex + ENEMIGO_SIZE)):
		if (ey >= jy and ey <(jy + JUGADOR_SIZE)) or (jy >= ey and jy <(ey + ENEMIGO_SIZE)):
			return true
	return False

while not game_over:
	#mapear los eventos dentro de la pantalla
	for event in pygame.event.get():
		print(event)
		if event.type == pygame.QUIT:
			sys.exit()

	#Movimientos en la pantalla
		if event.type == pygame.KEYDOWN:
			x = JUGADOR_POS[0]
			if event.key == pygame.K_LEFT:
				x -= JUGADOR_SIZE
			if event.key == pygame.K_RIGHT:
				x += JUGADOR_SIZE

			JUGADOR_POS[0] = x

	ventana.fill(COLOR_NEGRO)

	if ENEMIGO_POS[1] >= 0 and ENEMIGO_POS[1] < LARGO:
		ENEMIGO_POS[1] += 20
	else:
		ENEMIGO_POS[0] = random.randint(0, ANCHO - ENEMIGO_SIZE)
		ENEMIGO_POS[1] = 0

	#mapear colisiones
	if detectar_colision(JUGADOR_POS,ENEMIGO_POS):
		game_over = True

	#mapear enemigo
	pygame.draw.rect(ventana,(COLOR_AZUL),
					(ENEMIGO_POS[0],ENEMIGO_POS[1],
					ENEMIGO_SIZE,ENEMIGO_SIZE))

	#mapear el jugador
	pygame.draw.rect(ventana,(COLOR_ROJO),
					(JUGADOR_POS[0],JUGADOR_POS[1],
					JUGADOR_SIZE,JUGADOR_SIZE))
	clock.tick(30)
	pygame.display.update()